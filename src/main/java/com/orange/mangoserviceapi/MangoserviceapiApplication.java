package com.orange.mangoserviceapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MangoserviceapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MangoserviceapiApplication.class, args);
	}

}
