package com.orange.mangoserviceapi.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orange.mangoserviceapi.controller.domain.Cosmetic;

@RestController
public class CosmeticController {
	
	List<Cosmetic> cosmetics = new ArrayList<>(Arrays.asList(new Cosmetic("LIP001", "XOXO Lipstick", 100),
			new Cosmetic("BRO001", "NYX Brush On Palette", 200)));

	@RequestMapping("/cosmetics")
	public List<Cosmetic> getAllCosmetics() {
		return cosmetics;
	}
}