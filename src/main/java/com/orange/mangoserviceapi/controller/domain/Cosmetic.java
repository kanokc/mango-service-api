package com.orange.mangoserviceapi.controller.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Cosmetic {
	
	private String id;
	private String name;
	private int amount;

	public Cosmetic(String id, String name, int amount) {
		this.id = id;
		this.name = name;
		this.amount = amount;
	}

}